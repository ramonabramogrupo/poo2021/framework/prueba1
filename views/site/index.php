<?php

/* @var $this yii\web\View */

$this->title = 'Jota';
?>
<div class="site-index">
    <div class="body-content">

        <div class="row">
            <div class="col-lg-8 mx-auto">
                <h2 class="p-2 mx-auto my-4 border border-primary rounded text-center">Productos de Jota</h2>

                <div class="text-justify">
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ipsum, quam, esse ab labore provident laborum nemo ea. 
                    Impedit, est beatae rerum adipisci harum nam eum labore vitae fugit dolores ipsam.
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Rerum, eum, temporibus similique alias harum qui quia 
                    quo quaerat fuga optio commodi blanditiis repudiandae sequi reiciendis illo voluptas voluptate impedit possimus.
                </div>

            </div>
        </div>

    </div>
</div>
