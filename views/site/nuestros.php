<?php
use yii\bootstrap4\Carousel;

echo Carousel::widget([
    'items' => [
        // the item contains only the image
        '<img src="https://picsum.photos/800/600?random=1"/>',
        // equivalent to the above
        ['content' => '<img src="https://picsum.photos/800/600?random=2"/>'],
        // the item contains both the image and the caption
        [
            'content' => '<img src="https://picsum.photos/800/600?random=3"/>',
            'caption' => '<h4>This is title</h4><p>This is the caption text</p>',
        ],
    ],
    'options'=>[
        'class'=>'mx-auto col-lg-8'
    ],
    'controls' => ['<i class="fas fa-arrow-left fa-3x"></i>','<i class="fas fa-arrow-right fa-3x"></i>']
]);