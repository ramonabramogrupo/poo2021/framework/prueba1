<?php

return [
    'adminEmail' => 'admin@example.com',
    'senderEmail' => 'noreply@example.com',
    'senderName' => 'formulario de contacto',
    'informacion' => 'informacion@gmail.com',
    'contacto' => 'contacto@gmail.com',
    'bsVersion' => '4.x',
];

