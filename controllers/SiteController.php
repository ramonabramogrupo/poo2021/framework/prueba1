<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\Productos;
use yii\data\ActiveDataProvider;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionDonde()
    {
        return $this->render('donde');
    }
    
    public function actionQuienes()
    {
        return $this->render('quienes');
    }
    
    public function actionNuestros()
    {
        return $this->render('nuestros');
    }
    
    public function actionInformacion()
    {
        return $this->render('informacion');
    }
    

    public function actionOfertas()
    {

        $consulta=Productos::find()->where([
            "oferta" => 1
        ]);

        
        $dataProvider=new ActiveDataProvider([
            'query' => $consulta
        ]);
        
        return $this->render("ofertas",[
            "dataProvider" => $dataProvider,
        ]);
    }
    
    public function actionProductos()
    {
        
        $consulta=Productos::find();
        
        $dataProvider=new ActiveDataProvider([
            'query' => $consulta,
            'pagination' => [
                'pageSize' => 1,
            ],
        ]);
        
        return $this->render("productos",[
            "dataProvider" => $dataProvider,
        ]);
  
    }
    
    public function actionCategorias()
    {
        $consulta=Productos::find()->select("categorias")->distinct();
        
        $dataProvider=new ActiveDataProvider([
            'query' => $consulta,
        ]);
        
        return $this->render("categorias",[
            "dataProvider" => $dataProvider,
        ]);
    }
    
      
    public function actionContacto()
    {
    $model = new \app\models\Contacto(); // objeto de tipo contacto
        
    $datos=Yii::$app->request->post(); // leoo todos los datos enviados y los coloco en $datos
    
    if ($model->load($datos)) {
        if ($model->contact()) {
            /** chequear si has aceptado las politicas desde el controlador */
            /*if($model->politicas==0){
                $model->addError("politicas","No has aceptado las politicas");
            }else{
                return $this->render("mostrar");
            }*/
            
                return $this->render("mostrar");
                
        }
    }

    return $this->render('contacto', [
        'model' => $model,
    ]);
    }
    
    public function actionProductoscategoria($categoria){
        
        $consulta=Productos::find()->where([
            "categorias"=>$categoria
        ]);
        
                
        $dataProvider=new ActiveDataProvider([
            'query' => $consulta,
        ]);
        
        return $this->render("productoscategoria",[
            "dataProvider" => $dataProvider,
            "categoria" =>$categoria
        ]);        
        
    }
    
    
}
